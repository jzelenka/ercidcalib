# Measuring Energy-resolved collision-induced-dissociation on LCQ instruments

## Introduction
LCQ instruments belongs to quadrupole-ion-trap (Paul trap)[^fn1] mass
spectrometers. Among other things, they are capable of performing ms^n
experiments. Importantly, Detlef Schröder found out, that this instrument can
be easily used to determine the bond dissociation energies.[^fn2] As the
excitation energy in LCQ is reported as normalized energy in %, it is important
to determine the correlation between normalized energies (%) and absolute
energies (eV/particle, or kJ/mol).

This page should serve as a step-by-step guide how to use the LCQ instrument to
obtain reasonably precise bond dissociation energies - to perform the real life
measurement. If tempted, feel free to pursue the citations for deeper
understanding of the underlying theory.

## Mass-selection on the LCQ
To perform a mass selection on the LCQ you need to define a set of parameters shown in the Figure 1.

TODO: FIGURE

TODO: SOURCE OF THE DESCRIPTION (LCQ Manuals)

#### Parent Mass (m/z)
This parameter determines roughly the centre of the mass selection interval. It should be close to the target mass. It should be used to shift the selection window left-right. **It does not have to be exactly the mass of the selected ion**

#### Isolation width (m/z)
This parameter determines **roughly** the width of the m/z region which is selected.

#### Normalized collision energy (%)
This is the parameter we will later convert to the absolute collision energies. For mass selection itself, it is left to zero.

#### Activation Q
This parameter sets the amplitude of the activation during collisions. We keep it fixed on 0.250 for the CID experiments.

#### Activation Time (msec)
This parameter sets for how long the activation pulse is applied. We keep it on 20 milliseconds.

### Mass selection vs Activation
For energy resolved CID we would like to 1) isolate single m/z and exclude all
the other ions, 2) do not excite the desired m/z at all. Unfortunately, these
two goals are opposing to each-other. On the LCQ, there is not a sharp boundary
between activation and mass selection of the ions. What it means is that when
you're expelling some ions, you are actually exciting ions which are close to
them on the m/z scale. As a result, if you set the isolation width too narrow,
you tend to excite the m/z which you're still keeping in the trap. If you put
it too wide, your desired m/z is relaxed, but you then leave also the undesired
m/z in the trap.

The way out of this dilema is to set selection width and its centre reasonably
and sensitively. Figure 2 describes roughly how the selection
should look like. By this technique you usually end-up with an isolation width
somewhere between 1.5 and 3.

## Fragmentation in the LCQ
Once ions are cautiously selected, the fragmentation itself is
straight-forward. You just increase the normalized collision energy enough and
then you start to observe fragments.

## Calibrating LCQ

### Thermometer ions
There is a set of ions which has been carefully measured by Armentrout et.
al.[^fn3] The bond dissociation energies of these ions is determined with great
care with a different experimental technique called Threshold CID (*TCID*). In
our case we use these well characterized ions to determine the scaling factor
for the LCQ Normalized collision energy. Through calibration curve. As these
ions are fragile, it is wise to keep the capillary temperature low (<=200°C) 

### Appearance Energies
The fragmentation of the ions is usually gradual and its related to the energy
applied. The so-called breakdown curve (dependency of the fragmentation in
relation to energy) is shown in Figure 3 and it has a sigmoid-like shape. The
best description of the energy is obtained through so-called Appearance energy
(*AE*). We get appearance energy by putting a tangent line into the inflex
point of the sigmoid curve (See Figure 3 for better understanding).

### Calibration curve
By comparing the appearance energies (*AE*, %NCE) with the threshold CID
(*TCID*, eV/particle) of various ions we obtain a factor through which we have
to multiply the energies from LCQ to obtain absolute energies. Example of such
calibration curve is shon in the Figure 4. As the calibration is dependent on
multiple parameters (Activation Q, Activation time, pressure of the helium in
the trap and so on), it is important to measure a new calibration curve each
week, or upon every parameter change/manipulation with the instrument. Based on
our observation, this calibration ratio drifts over the time.

## Determining bond dissociation energies for unknown ions
Once we have a recent calibration curve, the last step is to measure the
fragmentation of our ion of interest upon varying Normalized collision
energies. From that we obtain the breakdown curve and *AE* subsequently. By
multiplying with ratio obtained from the calibration, we receive the absolute
value of the activation energy.

[Calibrating LCQ - step-by-step](lcqcal.md)

 
## Citations

[^fn1]: https://youtu.be/Pj_Al9c9Yws?t=1890

[^fn2]: E.-L. Zins, C. Pepe, D. Schröder, J. Mass Spectrom. 2010, 45, 1253–1260. [doi.org/10.1002/jms.1847](https://doi.org/10.1002/jms.1847)

[^fn3]: TODO
